# DNS_Blackhole
Role to create and manage DNS zones that lead to nowhere (127.0.0.1).
Use case might be to block porn site on your network.

Role Variables
--------------

| Variable  | Type  |Default  | Descirption  |
|---|---|---|---|
|dns_blackhole_domains|list|-|Yaml List of domains to be pointed to 127.0.0.1 via DNS server.|
|dns_blackhole_pathToConf|String|/etc/named|Path to the location of the named config file this play will create|
|dns_blackhole_ConfName|String|named.blackhole.conf|Name of the config file this play will create|
|dns_blackhole_pathTozone|string|/etc/named/zones|Path to the location of the zone file this play will create|
|dns_blackhole_zoneName|string|blackhole.zone|Name of the zone file this play will create|



Example Playbook
----------------

```yaml
- name: Create Blackhole
  hosts: all
  become: yes
  vars:
```


Notes
-----


To Do
-----
Create variables
Create config template
create zone template


License
-------

BSD

Author Information
------------------

Clayton Black
cblack@claytontblack.com
http://www.claytontblack.com